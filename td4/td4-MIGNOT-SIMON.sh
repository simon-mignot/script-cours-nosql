#!/bin/bash

json_customers="$(cat <<-'EOF'
{customer_num:101,fname:"Ludwig",lname:"Pauli",company:"All Sports Supplies",address1:"213 Erstwild Court",city:"Sunnyvale",state:"CA",zipcode:"94086",phone:"408-789-8075"}
{customer_num:102,fname:"Carole",lname:"Sadler",company:"Sports Spot",address1:"785 Geary St",city:"San Francisco",state:"CA",zipcode:"94117",phone:"415-822-1289"}
{customer_num:103,fname:"Philip",lname:"Currie",company:"Phil's Sports",address1:"654 Poplar",address2:"P. O. Box 3498",city:"Palo Alto",state:"CA",zipcode:"94303",phone:"415-328-4543"}
{customer_num:104,fname:"Anthony",lname:"Higgins",company:"Play Ball!",address1:"East Shopping Cntr.",address2:"422 Bay Road",city:"Redwood City",state:"CA",zipcode:"94026",phone:"415-368-1100"}
{customer_num:105,fname:"Raymond",lname:"Vector",company:"Los Altos Sports",address1:"1899 La Loma Drive",city:"Los Altos",state:"CA",zipcode:"94022",phone:"415-776-3249"}
{customer_num:106,fname:"George",lname:"Watson",company:"Watson & Son",address1:"1143 Carver Place",city:"Mountain View",state:"CA",zipcode:"94063",phone:"415-389-8789"}
{customer_num:107,fname:"Charles",lname:"Ream",company:"Athletic Supplies",address1:"41 Jordan Avenue",city:"Palo Alto",state:"CA",zipcode:"94304",phone:"415-356-9876"}
{customer_num:108,fname:"Donald",lname:"Quinn",company:"Quinn's Sports",address1:"587 Alvarado",city:"Redwood City",state:"CA",zipcode:"94063",phone:"415-544-8729"}
{customer_num:109,fname:"Jane",lname:"Miller",company:"Sport Stuff",address1:"Mayfair Mart",address2:"7345 Ross Blvd.",city:"Sunnyvale",state:"CA",zipcode:"94086",phone:"408-723-8789"}
{customer_num:110,fname:"Roy",lname:"Jaeger",company:"AA Athletics",address1:"520 Topaz Way",city:"Redwood City",state:"CA",zipcode:"94062",phone:"415-743-3611"}
{customer_num:111,fname:"Frances",lname:"Keyes",company:"Sports Center",address1:"3199 Sterling Court",city:"Sunnyvale",state:"CA",zipcode:"94085",phone:"408-277-7245"}
{customer_num:112,fname:"Margaret",lname:"Lawson",company:"Runners & Others",address1:"234 Wyandotte Way",city:"Los Altos",state:"CA",zipcode:"94022",phone:"415-887-7235"}
{customer_num:113,fname:"Lana",lname:"Beatty",company:"Sportstown",address1:"654 Oak Grove",city:"Menlo Park",state:"CA",zipcode:"94025",phone:"415-356-9982"}
{customer_num:114,fname:"Frank",lname:"Albertson",company:"Sporting Place",address1:"947 Waverly Place",city:"Redwood City",state:"CA",zipcode:"94062",phone:"415-886-6677"}
{customer_num:115,fname:"Alfred",lname:"Grant",company:"Gold Medal Sports",address1:"776 Gary Avenue",city:"Menlo Park",state:"CA",zipcode:"94025",phone:"415-356-1123"}
{customer_num:116,fname:"Jean",lname:"Parmelee",company:"Olympic City",address1:"1104 Spinosa Drive",city:"Mountain View",state:"CA",zipcode:"94040",phone:"415-534-8822"}
{customer_num:117,fname:"Arnold",lname:"Sipes",company:"Kids Korner",address1:"850 Lytton Court",city:"Redwood City",state:"CA",zipcode:"94063",phone:"415-245-4578"}
{customer_num:118,fname:"Dick",lname:"Baxter",company:"Blue Ribbon Sports",address1:"5427 College",city:"Oakland",state:"CA",zipcode:"94609",phone:"415-655-0011"}
{customer_num:119,fname:"Bob",lname:"Shorter",company:"The Triathletes Club",address1:"2405 Kings Highway",city:"Cherry Hill",state:"NJ",zipcode:"08002",phone:"609-663-6079"}
{customer_num:120,fname:"Fred",lname:"Jewell",company:"Century Pro Shop",address1:"6627 N. 17th Way",city:"Phoenix",state:"AZ",zipcode:"85016",phone:"602-265-8754"}
{customer_num:121,fname:"Jason",lname:"Wallack",company:"City Sports",address1:"Lake Biltmore Mall",address2:"350 W. 23rd Street",city:"Wilmington",state:"DE",zipcode:"19898",phone:"302-366-7511"}
{customer_num:122,fname:"Cathy",lname:"O'Brian",company:"The Sporting Life",address1:"543 Nassau Street",city:"Princeton",state:"NJ",zipcode:"08540",phone:"609-342-0054"}
{customer_num:123,fname:"Marvin",lname:"Hanlon",company:"Bay Sports",address1:"10100 Bay Meadows Ro",address2:"Suite 1020",city:"Jacksonville",state:"FL",zipcode:"32256",phone:"904-823-4239"}
{customer_num:124,fname:"Chris",lname:"Putnum",company:"Putnum's Putters",address1:"4715 S.E. Adams Blvd",address2:"Suite 909C",city:"Bartlesville",state:"OK",zipcode:"74006",phone:"918-355-2074"}
{customer_num:125,fname:"James",lname:"Henry",company:"Total Fitness Sports",address1:"1450 Commonwealth Av",city:"Brighton",state:"MA",zipcode:"02135",phone:"617-232-4159"}
{customer_num:126,fname:"Eileen",lname:"Neelie",company:"Neelie's Discount Sp",address1:"2539 South Utica Str",city:"Denver",state:"CO",zipcode:"80219",phone:"303-936-7731"}
{customer_num:127,fname:"Kim",lname:"Satifer",company:"Big Blue Bike Shop",address1:"Blue Island Square",address2:"12222 Gregory Street",city:"Blue Island",state:"NY",zipcode:"60406",phone:"312-944-5691"}
{customer_num:128,fname:"Frank",lname:"Lessor",company:"Phoenix University",address1:"Athletic Department",address2:"1817 N. Thomas Road",city:"Phoenix",state:"AZ",zipcode:"85008",phone:"602-533-1817"}
EOF
)"

json_orders="$(cat <<-'EOF'
{order_num:1001,order_date:"2008-05-20",customer_num:104,ship_instruct:"express",backlog:"n",po_num:"B77836",ship_date:"2008-06-01",ship_weight:20.40,ship_charge:"$10.00",paid_date:"2008-07-22"}
{order_num:1002,order_date:"2008-05-21",customer_num:101,ship_instruct:"PO on box; deliver to back door only",backlog:"n",po_num:"9270",ship_date:"2008-05-26",ship_weight:50.60,ship_charge:"$15.30",paid_date:"2008-06-03"}
{order_num:1003,order_date:"2008-05-22",customer_num:104,ship_instruct:"express",backlog:"n",po_num:"B77890",ship_date:"2008-05-23",ship_weight:35.60,ship_charge:"$10.80",paid_date:"2008-06-14"}
{order_num:1004,order_date:"2008-05-22",customer_num:106,ship_instruct:"ring bell twice",backlog:"y",po_num:"8006",ship_date:"2008-05-30",ship_weight:95.80,ship_charge:"$19.20"}
{order_num:1005,order_date:"2008-05-24",customer_num:116,ship_instruct:"call before delivery",backlog:"n",po_num:"2865",ship_date:"2008-06-09",ship_weight:80.80,ship_charge:"$16.20",paid_date:"2008-06-21"}
{order_num:1006,order_date:"2008-05-30",customer_num:112,ship_instruct:"after 10 am",backlog:"y",po_num:"Q13557",ship_weight:70.80,ship_charge:"$14.20"}
{order_num:1007,order_date:"2008-05-31",customer_num:117,backlog:"n",po_num:"278693",ship_date:"2008-06-05",ship_weight:125.90,ship_charge:"$25.20"}
{order_num:1008,order_date:"2008-06-07",customer_num:110,ship_instruct:"closed Monday",backlog:"y",po_num:"LZ230",ship_date:"2008-07-06",ship_weight:45.60,ship_charge:"$13.80",paid_date:"2008-07-21"}
{order_num:1009,order_date:"2008-06-14",customer_num:111,ship_instruct:"next door to grocery",backlog:"n",po_num:"4745",ship_date:"2008-06-21",ship_weight:20.40,ship_charge:"$10.00",paid_date:"2008-08-21"}
{order_num:1010,order_date:"2008-06-17",customer_num:115,ship_instruct:"deliver 776 King St. if no answer",backlog:"n",po_num:"429Q",ship_date:"2008-06-29",ship_weight:40.60,ship_charge:"$12.30",paid_date:"2008-08-22"}
{order_num:1011,order_date:"2008-06-18",customer_num:104,ship_instruct:"express",backlog:"n",po_num:"B77897",ship_date:"2008-07-03",ship_weight:10.40,ship_charge:"$5.00",paid_date:"2008-08-29"}
{order_num:1012,order_date:"2008-06-18",customer_num:117,backlog:"n",po_num:"278701",ship_date:"2008-06-29",ship_weight:70.80,ship_charge:"$14.20"}
{order_num:1013,order_date:"2008-06-22",customer_num:104,ship_instruct:"express",backlog:"n",po_num:"B77930",ship_date:"2008-07-10",ship_weight:60.80,ship_charge:"$12.20",paid_date:"2008-07-31"}
{order_num:1014,order_date:"2008-06-25",customer_num:106,ship_instruct:"ring bell, kick door loudly",backlog:"n",po_num:"8052",ship_date:"2008-07-03",ship_weight:40.60,ship_charge:"$12.30",paid_date:"2008-07-10"}
{order_num:1015,order_date:"2008-06-27",customer_num:110,ship_instruct:"closed Mondays",backlog:"n",po_num:"MA003",ship_date:"2008-07-16",ship_weight:20.60,ship_charge:"$6.30",paid_date:"2008-08-31"}
{order_num:1016,order_date:"2008-06-29",customer_num:119,ship_instruct:"delivery entrance off Camp St.",backlog:"n",po_num:"PC6782",ship_date:"2008-07-12",ship_weight:35.00,ship_charge:"$11.80"}
{order_num:1017,order_date:"2008-07-09",customer_num:120,ship_instruct:"North side of clubhouse",backlog:"n",po_num:"DM354331",ship_date:"2008-07-13",ship_weight:60.00,ship_charge:"$18.00"}
{order_num:1018,order_date:"2008-07-10",customer_num:121,ship_instruct:"SW corner of Biltmore Mall",backlog:"n",po_num:"S22942",ship_date:"2008-07-13",ship_weight:70.50,ship_charge:"$20.00",paid_date:"2008-08-06"}
{order_num:1019,order_date:"2008-07-11",customer_num:122,ship_instruct:"closed til noon Mondays",backlog:"n",po_num:"Z55709",ship_date:"2008-07-16",ship_weight:90.00,ship_charge:"$23.00",paid_date:"2008-08-06"}
{order_num:1020,order_date:"2008-07-11",customer_num:123,ship_instruct:"express",backlog:"n",po_num:"W2286",ship_date:"2008-07-16",ship_weight:14.00,ship_charge:"$8.50",paid_date:"2008-09-20"}
{order_num:1021,order_date:"2008-07-23",customer_num:124,ship_instruct:"ask for Elaine",backlog:"n",po_num:"C3288",ship_date:"2008-07-25",ship_weight:40.00,ship_charge:"$12.00",paid_date:"2008-08-22"}
{order_num:1022,order_date:"2008-07-24",customer_num:126,ship_instruct:"express",backlog:"n",po_num:"W9925",ship_date:"2008-07-30",ship_weight:15.00,ship_charge:"$13.00",paid_date:"2008-09-02"}
{order_num:1023,order_date:"2008-07-24",customer_num:127,ship_instruct:"no deliveries after 3 p.m.",backlog:"n",po_num:"KF2961",ship_date:"2008-07-30",ship_weight:60.00,ship_charge:"$18.00",paid_date:"2008-08-22"}
EOF
)"

json_calls="$(cat <<-'EOF'
{customer_num:106,call_dtime:"2008-06-12 08:20",user_id:"maryj",call_code:"D",call_descr:"Order was received, but two of the cans of ANZ tennis balls within the case were empty",res_dtime:"2008-06-12 08:25",res_descr:"Authorized credit for two cans to customer, issued apology. Called ANZ buyer to report the QA problem."}
{customer_num:110,call_dtime:"2008-07-07 10:24",user_id:"richc",call_code:"L",call_descr:"Order placed one month ago (6/7) not received.",res_dtime:"2008-07-07 10:30",res_descr:"Checked with shipping (Ed Smith). Order sent yesterday- we were waiting for goods from ANZ. Next time will call with delay if necessary."}
{customer_num:119,call_dtime:"2008-07-01 15:00",user_id:"richc",call_code:"B",call_descr:"Bill does not reflect credit from previous order",res_dtime:"2008-07-02 08:21",res_descr:"Spoke with Jane Akant in Finance. She found the error and is sending new bill to customer"}
{customer_num:121,call_dtime:"2008-07-10 14:05",user_id:"maryj",call_code:"O",call_descr:"Customer likes our merchandise. Requests that we stock more types of infant joggers. Will call back to place order.",res_dtime:"2008-07-10 14:06",res_descr:"Sent note to marketing group of interest in infant joggers"}
{customer_num:127,call_dtime:"2008-07-31 14:30",user_id:"maryj",call_code:"I",call_descr:"Received Hero watches (item # 304) instead of ANZ watches",res_descr:"Sent memo to shipping to send ANZ item 304 to customer and pickup HRO watches. Should be done tomorrow, 8/1"}
{customer_num:116,call_dtime:"2007-11-28 13:34",user_id:"mannyn",call_code:"I",call_descr:"Received plain white swim caps (313 ANZ) instead of navy with team logo (313 SHM)",res_dtime:"2007-11-28 16:47",res_descr:"Shipping found correct case in warehouse and express mailed it in time for swim meet."}
{customer_num:116,call_dtime:"2007-12-21 11:24",user_id:"mannyn",call_code:"I",call_descr:"Second complaint from this customer! Received two cases right-handed outfielder gloves (1 HRO) instead of one case lefties.",res_dtime:"2007-12-27 08:19",res_descr:"Memo to shipping (Ava Brown) to send case of left-handed gloves, pick up wrong case; memo to billing requesting 5% discount to placate customer due to second offense and lateness of resolution because of holiday"}
EOF
)"


#json_customers
#json_orders
#json_calls

DEFAULT_DBNAME="td4-db-MIGNOT-SIMON"
if [[ "$1" == "--help" ]]; then
    echo "Usage: $0 [db-name]"
    echo "Option:"
    echo "    db-name    default value: $DEFAULT_DBNAME"
    exit 0
fi

DBNAME=${1:-"$DEFAULT_DBNAME"}
OUTPUT='td4-MIGNOT-SIMON.log'
TOTAL_SCRIPT=0
SUCCESS_SCRIPT=0

#
# $1: String, title
# $2: String, script to be executed
# $3: Boolean, output $2 instead of script result
#
function jseval()
{
    ((TOTAL_SCRIPT++))
    if [[ -n "$1" && -n "$2" ]]; then
        title="\n\n==== $1"
        script="$2"
        output="$OUTPUT"
    else
        script="$1"
        output='/dev/null'
    fi

    result=$(echo $script | mongo ${DBNAME} --quiet)
    code="$?"
    echo -ne "$title " >> "$output"
    if [[ "$code" == 0 && "$result" != 'false' ]]; then
        ((SUCCESS_SCRIPT++))
        echo $'[\u2713]' >> "$output"
    else
        echo $'[\u2718]' >> "$output"
    fi
    if [[ -n "$3" ]]; then
        echo "$script" >> "$output"
    fi
    echo "$result" >> "$output"
}

# Clear output file
rm $OUTPUT

echo "Using db $DBNAME."
echo "Using db $DBNAME." >> "$OUTPUT"

# Reset database
mongo $DBNAME --quiet --eval "db.dropDatabase();"
echo -n "$json_customers" | tr -d '\n' | mongoimport -d ${DBNAME} -c "customers"
echo -n "$json_orders" | tr -d '\n' | mongoimport -d ${DBNAME} -c "orders"
echo -n "$json_calls" | tr -d '\n' | mongoimport -d ${DBNAME} -c "calls"

jseval '1.a    SELECT * FROM customers WHERE company MATCHES "*Sports*" AND company NOT MATCHES "*town*"' 'db.customers.find({ $and: [{ "company": /.*Sports.*/ }, { "company": { $not: /.*town.*/ } }] }).length() == 11' true

jseval '1.b    SELECT * FROM calls WHERE call_descr MATCHES "*hero watch*" OR call_descr MATCHES "*tennis*"' 'db.calls.find({ $or: [{ "call_descr": /.*hero watch.*/ }, { "call_descr": /.*tennis.*/ }] }).length() == 1' true

jseval '2.a    db.customers.find( (address2 = null) );' 'db.customers.find({ "address2": null }).length() == 20' true

jseval '2.b    db.orders.find(( pai_date: ( $type = 11 )))' 'db.orders.find({ "paid_date": { $type: 11 } }).length() == 0' true

jseval '3.a    db.customer.find ( ( state :"CA", fname:"Arnold"))' 'db.customers.find({ "state": "CA", "fname": "Arnold" }, { _id: 1, fname: 1, lname: 1 }).pretty()' true

jseval '3.b    db.customer.find ( ( zipcod :’85008’))' 'db.customers.find({ "zipcode": "85008" }, { _id: 1, fname: 1, lname: 1 }).pretty()' true
